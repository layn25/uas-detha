<?php 
	include'koneksi.php';
 ?>
<html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Sign In</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">



<link rel="stylesheet" href="styles/login_style.css">


</head>
<body>
<!-- partial:index.partial.html -->
	<div class="box-form">
		<div class="left">
			<div class="overlay">
				<h1>Connect With Us</h1>
				<p>Login to receive our latest deal and get yourself a discount for a few special electronic device</p>
			</div>
		</div>
		
		<div class="right">
			<form method="POST" action="proses/signin_proses.php">
				<h5>Sign In</h5>
				<p>Have an active account? <a href="login.php">Use My Account</a> Shouldn't be harder than that</p>

				<div class="inputs">

					<input type="text" name="nama" placeholder="nama">
					<br>
					<input type="text" name="email" placeholder="email">
					<br>
					<input type="text" name="username"  placeholder="username">
					<br>
					<input type="password" name="password" placeholder="password">
					<br>
					<input type="text" name="telp" placeholder="Nomor Telephone">

				</div>
					
				<br><br>
					
				<div class="remember-me--forget-password">
						<!-- Angular -->
					<label>
						<input type="checkbox" name="item" checked/>
						<span class="text-checkbox">Remember me</span>
					</label>
							<p>forget password?</p>
				</div>
				<br>
				<button type="submit">Sign In</button>
			</form>
		</div>
	</div>
<!-- partial -->
  
</body>
</html>
