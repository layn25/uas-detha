<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>GG Admin</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">


  <div class="card shadow">
    <div class="card-header">
      Edit Produk
    </div>


    <div class="card-body">

      <?php 
          require '../koneksi.php';
          $sql=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE produk_id='$_GET[id]'");
          if ($data=mysqli_fetch_array($sql)) {

          
      ?>

      <form action="index_produk_proses.php" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="form-group cols-sm-6">
          <label>Id Produk</label>
          <input type="text" name="produk_id" value="<?php echo $data['produk_id'];?>" class="form-control" readonly>
        </div>
        <div class="form-group cols-sm-6">
          <label>Id Kategori</label>
          <input type="text" name="kat_id" value="<?php echo $data['kat_id'];?>" class="form-control" readonly>
        </div>
        <div class="form-group cols-sm-6">
          <label>nama</label>
          <input type="text" name="nama" value="<?php echo $data['nama'];?>" class="form-control">
        </div>
        <div class="form-group cols-sm-6">
          <label>Deskripsi</label>
          <textarea name="deskripsi" value="" class="form-control" style="height: 250px"> <?php echo $data['deskripsi']; ?> </textarea>
        </div>
        <div class="form-group cols-sm-6">
          <label>Id Produk</label>
          <input type="text" name="harga" value="<?php echo $data['harga'];?>" class="form-control">
        </div>
        <div class="form-group cols-sm-6">
          <label>Image</label><br>
          <img src="../uploadedimg/<?php echo $data ['img'] ?>" style="width: 500px" ><br>
         
        </div>
        
        
        <div class="form-group cols-sm-6">
          <input type="submit" value="Simpan" class="btn btn-success">
          <input type="reset" value="batal" class="btn btn-danger">
        </div>


      </form>
      <?php 
        

}
       ?>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

</body>

</html>