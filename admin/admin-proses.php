<?php 
if (isset($_GET['url']))
{
	$url=$_GET['url'];

	switch($url)
	{
		case 'tambah_produk';
		include 'tambah_produk.php';
		break;

		case 'edit_data';
		include 'edit_produk.php';
		break;

		case 'edit-data-jurnalis';
		include 'edit-data-jurnalis.php';
		break;

    case 'index_edit';
    include 'index_produk.php';
    break;
		
	}
}

else {	
 ?>

<style type="text/css">
  th{
    text-align: center;
  }
</style>
<div class="card shadow mb-4">
            <div class="card-header py-3" style="color: #1a1a1a">
              <h6 class="m-0 font-weight-bold ">Data Produk</h6>
            </div>
            <div class="card-body">

              
             
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id Produk</th>
                      <th>Nama Produk</th>
                      <th style="width: 50%;">Deskripsi</th>
                      <th>Harga Produk</th>
                      <th>Gambar</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  <?php
                  require '../koneksi.php';
                  $sql=mysqli_query($koneksi,"SELECT * FROM tb_produk");
                  while ($data=mysqli_fetch_array($sql)) {

                  ?>


                  <tbody>
                    <tr>
                      <td> <?php echo $data['produk_id']; ?></td>
                      <!-- <td> <?php echo $data['id_jurnalis']; ?></td> -->
                      <td> <?php echo $data['nama']; ?></td>
                      <td> <?php echo $data['deskripsi']; ?>
                      <td> <?php echo $data['harga']; ?>
                      <td> <img src="../uploadedimg/<?php echo $data['img']; ?>" style="width: 200px" ></td>
                      <td>
                        
                        <a href="index.php?url=index_edit&id=<?php echo $data['produk_id']; ?>" class="btn btn-success btn-icon-split">
                          <span class="text">Edit</span>
                        </a>

                        <a href="delete_produk.php?id=<?php echo $data['produk_id']; ?>" class="btn btn-danger btn-icon-split" onclick="return confirm('Yakin ingin menghapus data ini?');">
                          <span class="text">Delete</span>
                        </a>



                      </td>
                    </tr>
                   
                  </tbody>
                  <?php } ?>
                </table>
              </div>

        

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->

  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>




<?php
}
?>