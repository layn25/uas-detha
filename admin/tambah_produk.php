<?php 
    session_start();
	include'../koneksi.php';
	/*$id = $_SESSION['id_jurnalis'];*/
    /*$username = $_SESSION['username'];*/
 ?>
<html>
<head>
	<title>Tambah Produk</title>
</head>
<link rel="stylesheet" type="text/css" href="../styles/tambah_produk.css">
<link href="../styles/bootstrap4/bootstrap.min.css" rel="stylesheet">

<body>

	<div class="container">
		<div class="rows">	
			<div class="col-lg-12 ">	
				<div class="formdata " >
					<form method="POST" action="tambah_produk_proses.php" enctype="multipart/form-data">
						<div class="indata">							
							<h1 >Tambah Produk Good Gadget</h1>
						</div>
						<div class="indata">
							<!-- <input type="text" name="kat_id" placeholder="kategori"> -->
							<select class="minimal" name="kategori">
								<?php 
									$query= "SELECT * FROM tb_kategori ";
									$pla=mysqli_query($koneksi, $query);
									if (!$pla) {
						                die("Query Error :".mysqli_errno($koneksi). " - ".mysqli_error($koneksi));
						             }
						              while ($row = mysqli_fetch_assoc($pla)) 
						             {
								?>
								<option  value="<?php echo $row['kat_id']?>">
									<?php echo $row['kat_nama']?>
								</option>
								<?php }?>
							</select>
						</div>
						<div class="indata">
							<input type="text" name="nama" placeholder="Nama Produk">
						</div>
						<div class="indata"> 
							<textarea cols="56" rows="5" name="deskripsi" placeholder="Deskripsi"></textarea>
						</div>
						<div class="indata">
							<input type="text" name="harga" placeholder="Harga">
						</div>
						<div class="indata"> 
							<input type="file" name="foto" accept=".jpg, jpeg, .png, .gif">
						</div>
						<div class="indata ">
							<button type="submit" class="submit">Tambah Produk</button>	
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>