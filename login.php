<?php 
	include'koneksi.php';
 ?>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Log In</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="styles/login_style.css">

</head>
<body>
<!-- partial:index.partial.html -->
	<div class="box-form">
		<div class="left">
			<div class="overlay">
				<h1>Connect With Us</h1>
				<p>Login to receive our latest deal and get yourself a discount for a few special electronic device</p>
			</div>
		</div>
		
		<div class="right">
			<h5>Login</h5>
			<p>Don't have an account? <a href="signin.php">Creat Your Account</a> it takes less than a minute</p>
			<form method="POST" action="proses/login_proses.php">
				
				<div class="inputs">
					<input type="text" name="username" placeholder="user name">
					<br>
					<input type="password" name="password" placeholder="password">		
				</div>
					
				<br><br>
					
				<div class="remember-me--forget-password">
						<!-- Angular -->
					<label>
						<input type="checkbox" name="item" checked/>
						<span class="text-checkbox">Remember me</span>
					</label>
							<p>forget password?</p>
				</div>
				<br>
				<button type="submit" class="submit">Login</button>
			</form>
		</div>
	</div>
<!-- partial -->
  
</body>
</html>
